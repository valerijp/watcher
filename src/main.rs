use std::path::Path;
use std::sync::mpsc::channel;
use notify::{EventKind, RecommendedWatcher, RecursiveMode, Watcher};
use clap::{Parser};
use glob::{MatchOptions, Pattern};
use std::process::Command;

#[derive(Parser)]
/// calls the <WORKER> when a new file matching the <PATTERN> is added to <DIRECTORY>
///
/// e.g. watcher /foo/bar *.webp webp2jpeg {}
struct Cli {
    #[clap(short, long)]
    verbose: bool,

    #[clap(short = 'i', long = "replace", default_value = "{}")]
    replacement_pattern: String,

    /// directory to observe.
    directory: String,

    /// glob pattern to check the filepath against
    pattern: String,

    /// worker to call
    ///
    /// use a path that is in $PATH. for adjacent executables use `./tool`
    worker: String,

    /// params to pass on to the worker.
    ///
    /// if none provided the path of watched file will be passed
    /// c.f. the --replace option
    worker_params: Vec<String>,
}

fn main() {
    let cli = Cli::parse();
    let directory = Path::new(&cli.directory).canonicalize().unwrap();

    if !directory.is_dir() {
        eprintln!("please pass a directory to observe");
        return;
    }

    let pattern = Pattern::new(&cli.pattern).unwrap();
    let options = MatchOptions {
        case_sensitive: false,
        require_literal_separator: false,
        require_literal_leading_dot: false,
    };

    if cli.verbose {
        println!("watching {}", directory.display());
    }

    let (tx, rx) = channel();

    // Automatically select the best implementation for your platform.
    // You can also access each implementation directly e.g. INotifyWatcher.
    let mut watcher = RecommendedWatcher::new(tx).expect("error while initializing watcher");

    // Add a path to be watched. All files and directories at that path and
    // below will be monitored for changes.
    match watcher.watch(cli.directory.as_ref(), RecursiveMode::Recursive) {
        Err(e) => eprintln!("startup error: {:?}", e),
        Ok(_) => (),
    };

    for res in rx {
        match res {
            Err(e) => println!("watch error: {:?}", e),
            Ok(event) => {
                match event.kind {
                    EventKind::Create(_) => {
                        for path in event.paths {
                            if pattern.matches_path_with(&*path, options) {
                                if cli.verbose {
                                    println!("created: {:?}", path);
                                }
                                let mut worker = Command::new(cli.worker.clone());

                                for worker_param in &cli.worker_params {
                                    if worker_param == &cli.replacement_pattern {
                                        worker.arg(&path)
                                    } else {
                                        worker.arg(worker_param)
                                    };
                                };

                                if cli.verbose {
                                    let args: Vec<&str> = worker.get_args().map(|s| s.to_str().unwrap_or("-MISSING-")).collect();
                                    println!("calling \"{}`\" {:?} ", worker.get_program().to_str().unwrap_or(&cli.worker), args)
                                }

                                match worker.status() {
                                    Ok(status) => {
                                        if cli.verbose {
                                            println!("worker exited with status: {:?}", status)
                                        }
                                    }
                                    Err(e) => { eprintln!("error running worker, {:?}", e) }
                                }
                            }
                        }
                    }
                    _ => {}
                }
            }
        }
    }
}
